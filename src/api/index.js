// @flow

// import axios from "axios";

// Define user's language. Different browsers have the user locale defined
// on different fields on the `navigator` object, so we make sure to account
// for these different by checking all of them
// const language = (() => {
//     if (typeof window === "undefined") {
//         return "en-US";
//     }

//     if (navigator.languages && navigator.languages.length) {
//         return navigator.languages[0];
//     }

//     return navigator.language || (navigator: any).userLanguage;
// })();

// export const create = () => {
//     const API_BASE = process.env.GATSBY_API_BASE;
//     if (!API_BASE) throw new ReferenceError("NO API DEFINED");

//     return axios.create({
//         baseURL: API_BASE,
//         headers: {
//             "Content-Type": "application/json",
//             "Accept-Language": language,
//         },
//         validateStatus: status => status >= 200 && status < 400,
//     });
// };
