// @flow

import { combineReducers } from "redux";

/**
 * The root reducer
 */
export default combineReducers<*, *>({});
