// // @flow

// import style from "./style.module.scss";

// import React from "react";
// import { GatsbyImage as Image } from "gatsby-plugin-image";
// import { useStaticQuery, graphql } from "gatsby";

// /**
//  * FocalPointImage
//  */
// const FocalPointImage = ({ imageData }: *) => {
//     const data = useStaticQuery(
//         graphql`
//             query {
//                 contentfulImageFocalPoint(
//                     image: {
//                         id: { eq: "1906d630-a40d-5fe4-b805-d63a6718cfe6" }
//                     }
//                 ) {
//                     image {
//                         file {
//                             details {
//                                 image {
//                                     height
//                                     width
//                                 }
//                             }
//                         }
//                     }
//                     focalPoint {
//                         focalPoint {
//                             x
//                             y
//                         }
//                     }
//                 }
//             }
//         `,
//     );
//     console.log(data);

//     let heightThird =
//         data.contentfulImageFocalPoint.image.file.details.image.height / 3;
//     let widthThird =
//         data.contentfulImageFocalPoint.image.file.details.image.width / 3;
//     let pointX = data.contentfulImageFocalPoint.focalPoint.focalPoint.x;
//     let pointY = data.contentfulImageFocalPoint.focalPoint.focalPoint.y;
//     let bottom,
//         top,
//         left,
//         right = false;
//     let focusString = "";

//     if (pointX >= widthThird) {
//         if (pointX < widthThird * 2) {
//             console.log("CENTER");
//         } else {
//             right = true;
//         }
//     } else if (pointX >= widthThird * 2) {
//         console.log("RIGHT");
//     } else {
//         left = true;
//     }

//     if (pointY >= heightThird) {
//         if (pointY < heightThird * 2) {
//             console.log("CENTER");
//         } else {
//             bottom = true;
//         }
//     } else if (pointY >= heightThird * 2) {
//         bottom = true;
//     } else {
//         top = true;
//     }

//     if (bottom) {
//         if (right) {
//             console.log("bottom_right");
//             focusString = "bottom_right";
//         } else if (left) {
//             console.log("bottom_left");
//             focusString = "bottom_left";
//         } else {
//             console.log("bottom");
//             focusString = "bottom";
//         }
//     } else if (top) {
//         if (right) {
//             console.log("top_right");
//             focusString = "top_right";
//         } else if (left) {
//             console.log("top_left");
//             focusString = "top_left";
//         } else {
//             console.log("top");
//             focusString = "top";
//         }
//     } else if (left) {
//         console.log("left");
//         focusString = "left";
//     } else {
//         console.log("right");
//         focusString = "right";
//     }

//     return (
//         <>
//             <p>
//                 original height:{" "}
//                 {data.contentfulImageFocalPoint.image.file.details.image.height}
//             </p>
//             <p>
//                 original width:{" "}
//                 {data.contentfulImageFocalPoint.image.file.details.image.width}
//             </p>
//             <p>
//                 focal point x:{" "}
//                 {data.contentfulImageFocalPoint.focalPoint.focalPoint.x}
//             </p>
//             <p>
//                 focal point y:{" "}
//                 {data.contentfulImageFocalPoint.focalPoint.focalPoint.y}
//             </p>
//             <div>
//                 <Image
//                     alt={"test"}
//                     className={style.bazookaImage}
//                     image={{
//                         width: 1,
//                         height: imageData.contentfulAsset.mobile.aspectRatio,
//                         layout: `fluid`,
//                         placeholder: {
//                             fallback: imageData.contentfulAsset.mobile.base64,
//                         },
//                         images: {
//                             fallback: {
//                                 src: `${imageData.contentfulAsset.mobile.src}&f=${focusString}`,
//                                 srcSet: imageData.contentfulAsset.mobile.srcSet.replaceAll(
//                                     "fill",
//                                     `fill&f=${focusString}`,
//                                 ),
//                                 sizes: imageData.contentfulAsset.mobile.sizes,
//                             },
//                             sources: [
//                                 {
//                                     srcSet: imageData.contentfulAsset.desktop.srcSetWebp.replaceAll(
//                                         "fill",
//                                         `fill&f=${focusString}`,
//                                     ),
//                                     type: "image/webp",
//                                     sizes:
//                                         imageData.contentfulAsset.desktop.sizes,
//                                     media: "(min-width: 768px)",
//                                 },
//                                 {
//                                     srcSet: imageData.contentfulAsset.desktop.srcSet.replaceAll(
//                                         "fill",
//                                         `fill&f=${focusString}`,
//                                     ),
//                                     sizes:
//                                         imageData.contentfulAsset.desktop.sizes,
//                                     media: "(min-width: 768px)",
//                                 },
//                                 {
//                                     srcSet: imageData.contentfulAsset.mobile.srcSetWebp.replaceAll(
//                                         "fill",
//                                         `fill&f=${focusString}`,
//                                     ),
//                                     type: "image/webp",
//                                     sizes:
//                                         imageData.contentfulAsset.mobile.sizes,
//                                 },
//                                 {
//                                     srcSet: imageData.contentfulAsset.mobile.srcSet.replaceAll(
//                                         "fill",
//                                         `fill&f=${focusString}`,
//                                     ),
//                                     sizes:
//                                         imageData.contentfulAsset.mobile.sizes,
//                                 },
//                             ],
//                         },
//                     }}
//                 />
//             </div>
//         </>
//     );
// };

// export default FocalPointImage;
