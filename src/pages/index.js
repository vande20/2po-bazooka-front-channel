// @flow
// import style from "../styles/index.scss";
import React from "react";
import { Container, Row, Col } from "reactstrap";
import { useStaticQuery, graphql } from "gatsby";
import { GatsbyImage as Image } from "gatsby-plugin-image";
import { getImageObject } from "../../utils/Images/index";
// import Img from "gatsby-image";

const HomePage = () => {
    const data = useStaticQuery(
        graphql`
            query {
                landscapeDesktop: contentfulImageWithFocalPoint(
                    id: { eq: "d41d33c5-db46-5da5-9591-9cfb1ab868c5" }
                ) {
                    fluidWithFocus(
                        maxWidth: 1140
                        maxHeight: 300
                        resizingBehavior: FILL
                    ) {
                        ...GatsbyContentfulFluidWithFocus
                    }
                }
                landscapeMobile: contentfulImageWithFocalPoint(
                    id: { eq: "d41d33c5-db46-5da5-9591-9cfb1ab868c5" }
                ) {
                    fluidWithFocus(
                        maxWidth: 510
                        maxHeight: 289
                        resizingBehavior: FILL
                    ) {
                        ...GatsbyContentfulFluidWithFocus
                    }
                }
                portraitDesktop: contentfulImageWithFocalPoint(
                    id: { eq: "fc6c14c5-04af-580d-94ea-f08ac453cbfc" }
                ) {
                    fluidWithFocus(
                        maxWidth: 600
                        maxHeight: 900
                        resizingBehavior: FILL
                    ) {
                        ...GatsbyContentfulFluidWithFocus
                    }
                }
                portraitMobile: contentfulImageWithFocalPoint(
                    id: { eq: "fc6c14c5-04af-580d-94ea-f08ac453cbfc" }
                ) {
                    fluidWithFocus(
                        maxWidth: 510
                        maxHeight: 480
                        resizingBehavior: FILL
                    ) {
                        ...GatsbyContentfulFluidWithFocus
                    }
                }
                squareDesktop: contentfulImageWithFocalPoint(
                    id: { eq: "3a7f40dc-e8f5-5be3-b3d5-8b97c6bf1b1f" }
                ) {
                    fluidWithFocus(
                        maxWidth: 250
                        maxHeight: 250
                        resizingBehavior: FILL
                    ) {
                        ...GatsbyContentfulFluidWithFocus
                    }
                }
                squareMobile: contentfulImageWithFocalPoint(
                    id: { eq: "3a7f40dc-e8f5-5be3-b3d5-8b97c6bf1b1f" }
                ) {
                    fluidWithFocus(
                        maxWidth: 510
                        maxHeight: 510
                        resizingBehavior: FILL
                    ) {
                        ...GatsbyContentfulFluidWithFocus
                    }
                }
            }
        `,
    );

    return (
        <Container>
            <Row>
                <Col xs={12}>
                    <h1>Focal point Images</h1>
                </Col>
                <Col xs={12}>
                    <h3 style={{ marginTop: "1rem" }}>Landscape</h3>
                    <div className="container-image-landscape">
                        <Image
                            className="bazooka-image-landscape"
                            image={getImageObject(
                                data.landscapeMobile.fluidWithFocus,
                                data.landscapeDesktop.fluidWithFocus,
                            )}
                            alt="test"
                        />
                    </div>
                    <h3 style={{ marginTop: "1rem" }}>Portrait</h3>
                    <div className="container-image-portrait">
                        <Image
                            className="bazooka-image-portrait"
                            image={getImageObject(
                                data.portraitMobile.fluidWithFocus,
                                data.portraitDesktop.fluidWithFocus,
                            )}
                            alt="test"
                        />
                    </div>

                    <h3 style={{ marginTop: "1rem" }}>Square</h3>
                    <div className="container-image-square">
                        <Image
                            className="bazooka-image-square"
                            image={getImageObject(
                                data.squareMobile.fluidWithFocus,
                                data.squareDesktop.fluidWithFocus,
                            )}
                            alt="test"
                        />
                    </div>
                </Col>
            </Row>
        </Container>
    );
};

export default HomePage;
