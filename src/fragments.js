import { graphql } from "gatsby";

/**
 * The simplest set of fields for fixed assets
 * @type {Fragment}
 * @example
 * myContentfulAssetField {
 *   fixed {
 *     ...GatsbyContentfulFixed
 *     # ^ identical to using the following fields:
 *     # base64
 *     # width
 *     # height
 *     # src
 *     # srcSet
 *   }
 * }
 */
export const GatsbyContentfulFluidWithFocus = graphql`
    fragment GatsbyContentfulFluidWithFocus on ContentfulFluidWithFocus {
        base64
        aspectRatio
        src
        srcSet
        srcWebp
        srcSetWebp
        sizes
    }
`;

export const GatsbyContentfulFixedWithFocus = graphql`
    fragment GatsbyContentfulFixedWithFocus on ContentfulFixedWithFocus {
        base64
        width
        height
        src
        srcSet
        srcWebp
        srcSetWebp
    }
`;
