/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
require("@babel/register");
module.exports = require("./lib");
const extendNodeType = require("./node_modules/gatsby-source-contentful/extend-node-type");
const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLFloat,
    GraphQLNonNull,
} = require(`gatsby/graphql`);
const {
    ImageFormatType,
    ImageResizingBehavior,
} = require(`./node_modules/gatsby-source-contentful/schemes`);

const { getTracedSVG, getBase64Image } = require("./utils/utils.js");

module.exports.setFieldsOnGraphQLNodeType = ({ type, getNode, store }) => {
    if (type.name === `ContentfulImageWithFocalPoint`) {
        const nodeResolver = (image, options, context, resolver) => {
            let imgNode = getNode(image.imageUrl___NODE);
            options = {
                ...options,
                cropFocus: image.focalPoint,
            };
            image = getNode(image.imageUrl___NODE);
            const node = resolver(imgNode, options);

            return {
                ...node,
                image,
                options,
                context,
            };
        };

        const base64Field = {
            type: GraphQLString,
            resolve(imageProps) {
                return getBase64Image(imageProps);
            },
        };

        const tracedSVGField = {
            type: GraphQLString,
            resolve({ image, options, context }) {
                return getTracedSVG({ image, options, context }, store);
            },
        };

        const srcWebpResolver = (image, options, isSrcSet, resolver) => {
            if (
                image?.file?.contentType === `image/webp` ||
                options.toFormat === `webp`
            ) {
                return null;
            }

            const resolvedImage = resolver(image, {
                ...options,
                toFormat: `webp`,
            });
            return isSrcSet ? resolvedImage?.srcSet : resolvedImage?.src;
        };

        return {
            fluidWithFocus: {
                type: new GraphQLObjectType({
                    name: "ContentfulFluidWithFocus",
                    fields: {
                        base64: base64Field,
                        tracedSVG: tracedSVGField,
                        aspectRatio: { type: GraphQLFloat },
                        src: { type: new GraphQLNonNull(GraphQLString) },
                        srcSet: { type: new GraphQLNonNull(GraphQLString) },
                        srcWebp: {
                            type: GraphQLString,
                            resolve({ image, options }) {
                                return srcWebpResolver(
                                    image,
                                    options,
                                    false,
                                    extendNodeType.resolveFluid,
                                );
                            },
                        },
                        srcSetWebp: {
                            type: GraphQLString,
                            resolve({ image, options }) {
                                return srcWebpResolver(
                                    image,
                                    options,
                                    true,
                                    extendNodeType.resolveFluid,
                                );
                            },
                        },
                        sizes: { type: new GraphQLNonNull(GraphQLString) },
                    },
                }),
                args: {
                    maxWidth: {
                        type: GraphQLInt,
                    },
                    maxHeight: {
                        type: GraphQLInt,
                    },
                    quality: {
                        type: GraphQLInt,
                        defaultValue: 50,
                    },
                    toFormat: {
                        type: ImageFormatType,
                        defaultValue: ``,
                    },
                    resizingBehavior: {
                        type: ImageResizingBehavior,
                    },
                    background: {
                        type: GraphQLString,
                        defaultValue: null,
                    },
                    sizes: {
                        type: GraphQLString,
                    },
                },
                resolve(image, options, context) {
                    return nodeResolver(
                        image,
                        options,
                        context,
                        extendNodeType.resolveFluid,
                    );
                },
            },
            fixedWithFocus: {
                type: new GraphQLObjectType({
                    name: "ContentfulFixedWithFocus",
                    fields: {
                        base64: base64Field,
                        tracedSVG: tracedSVGField,
                        aspectRatio: { type: GraphQLFloat },
                        width: { type: new GraphQLNonNull(GraphQLFloat) },
                        height: { type: new GraphQLNonNull(GraphQLFloat) },
                        src: { type: new GraphQLNonNull(GraphQLString) },
                        srcSet: { type: new GraphQLNonNull(GraphQLString) },
                        srcWebp: {
                            type: GraphQLString,
                            resolve({ image, options }) {
                                return srcWebpResolver(
                                    image,
                                    options,
                                    false,
                                    extendNodeType.resolveFixed,
                                );
                            },
                        },
                        srcSetWebp: {
                            type: GraphQLString,
                            resolve({ image, options }) {
                                return srcWebpResolver(
                                    image,
                                    options,
                                    true,
                                    extendNodeType.resolveFixed,
                                );
                            },
                        },
                    },
                }),
                args: {
                    width: {
                        type: GraphQLInt,
                    },
                    height: {
                        type: GraphQLInt,
                    },
                    maxWidth: {
                        type: GraphQLInt,
                    },
                    maxHeight: {
                        type: GraphQLInt,
                    },
                    quality: {
                        type: GraphQLInt,
                        defaultValue: 50,
                    },
                    toFormat: {
                        type: ImageFormatType,
                        defaultValue: ``,
                    },
                    resizingBehavior: {
                        type: ImageResizingBehavior,
                    },
                    background: {
                        type: GraphQLString,
                        defaultValue: null,
                    },
                },
                resolve(image, options, context) {
                    return nodeResolver(
                        image,
                        options,
                        context,
                        extendNodeType.resolveFixed,
                    );
                },
            },
        };
    }

    // by default return empty object
    return {};
};
