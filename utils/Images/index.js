// @flow

export const getImageObject = (mobile: *, desktop: *) => {
    return {
        width: 1,
        height: mobile.aspectRatio,
        layout: `fluid`,
        placeholder: {
            fallback: mobile.base64,
        },
        images: {
            fallback: {
                src: mobile.src,
                srcSet: mobile.srcSet,
                sizes: mobile.sizes,
            },
            sources: [
                {
                    srcSet: desktop.srcSetWebp,
                    type: "image/webp",
                    sizes: desktop.sizes,
                    media: "(min-width: 768px)",
                },
                {
                    srcSet: desktop.srcSet,
                    sizes: desktop.sizes,
                    media: "(min-width: 768px)",
                },
                {
                    srcSet: mobile.srcSetWebp,
                    type: "image/webp",
                    sizes: mobile.sizes,
                },
                {
                    srcSet: mobile.srcSet,
                    sizes: mobile.sizes,
                },
            ],
        },
    };
};
