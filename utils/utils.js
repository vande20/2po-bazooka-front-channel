require("@babel/register");
const fs = require(`fs`);
const path = require(`path`);
const crypto = require(`crypto`);
const base64Img = require(`base64-img`);
const { resolve, parse } = require(`path`);
const axios = require(`axios`);
const { pathExists, createWriteStream } = require(`fs-extra`);

const REMOTE_CACHE_FOLDER =
    process.env.GATSBY_CONTENTFUL_EXPERIMENTAL_REMOTE_CACHE ??
    process.env.GATSBY_REMOTE_CACHE ??
    path.join(process.cwd(), `.cache/remote_cache`);
const CACHE_IMG_FOLDER = path.join(REMOTE_CACHE_FOLDER, `images`);

// Promises that rejected should stay in this map. Otherwise remove promise and put their data in resolvedBase64Cache
const inFlightBase64Cache = new Map();
// This cache contains the resolved base64 fetches. This prevents async calls for promises that have resolved.
// The images are based on urls with w=20 and should be relatively small (<2kb) but it does stick around in memory
const resolvedBase64Cache = new Map();

const cacheImage = async function cacheImage(store, image, options) {
    const program = store.getState().program;
    const CACHE_DIR = resolve(`${program.directory}/.cache/contentful/assets/`);
    const {
        file: { url, fileName, details },
    } = image;
    const {
        width,
        height,
        maxWidth,
        maxHeight,
        resizingBehavior,
        cropFocus,
        background,
    } = options;
    const userWidth = maxWidth || width;
    const userHeight = maxHeight || height;

    const aspectRatio = details.image.height / details.image.width;
    const resultingWidth = Math.round(userWidth || 800);
    const resultingHeight = Math.round(
        userHeight || resultingWidth * aspectRatio,
    );

    const params = [`w=${resultingWidth}`, `h=${resultingHeight}`];
    if (resizingBehavior) {
        params.push(`fit=${resizingBehavior}`);
    }
    if (cropFocus) {
        //Changed source code to "f=" instead of "crop=" because of status 400 on crop
        params.push(`f=${cropFocus}`);
    }
    if (background) {
        params.push(`bg=${background}`);
    }

    const optionsHash = crypto
        .createHash(`md5`)
        .update(JSON.stringify([url, ...params]))
        .digest(`hex`);

    const { name, ext } = parse(fileName);
    const absolutePath = resolve(CACHE_DIR, `${name}-${optionsHash}${ext}`);

    const alreadyExists = await pathExists(absolutePath);

    if (!alreadyExists) {
        const previewUrl = `http:${url}?${params.join(`&`)}`;

        const response = await axios({
            method: `get`,
            url: previewUrl,
            responseType: `stream`,
        });

        await new Promise((resolve, reject) => {
            const file = createWriteStream(absolutePath);
            response.data.pipe(file);
            file.on(`finish`, resolve);
            file.on(`error`, reject);
        });
    }

    return absolutePath;
};

exports.getTracedSVG = async function getTracedSVG(args, store) {
    const { traceSVG } = require(`gatsby-plugin-sharp`);

    const { image, options } = args;
    const {
        file: { contentType },
    } = image;
    if (contentType.indexOf(`image/`) !== 0) {
        return null;
    }

    const absolutePath = await cacheImage(store, image, options);

    const extension = path.extname(absolutePath);

    options.cropFocus = "bottom";
    return traceSVG({
        file: {
            internal: image.internal,
            name: image.file.fileName,
            extension,
            absolutePath,
        },
        args: { toFormat: `` },
        fileArgs: options,
    });
};

exports.getBase64Image = function getBase64Image(imageProps) {
    if (!imageProps) {
        return null;
    }

    const requestUrl = `https:${imageProps.baseUrl}?w=20`;
    // Prefer to return data sync if we already have it
    const alreadyFetched = resolvedBase64Cache.get(requestUrl);
    if (alreadyFetched) {
        return alreadyFetched;
    }

    // If already in flight for this url return the same promise as the first call
    const inFlight = inFlightBase64Cache.get(requestUrl);
    if (inFlight) {
        return inFlight;
    }

    // Note: sha1 is unsafe for crypto but okay for this particular case
    const shasum = crypto.createHash(`sha1`);
    shasum.update(requestUrl);
    const urlSha = shasum.digest(`hex`);

    // TODO: Find the best place for this step. This is definitely not it.
    fs.mkdirSync(CACHE_IMG_FOLDER, { recursive: true });

    const cacheFile = path.join(CACHE_IMG_FOLDER, urlSha + `.base64`);

    if (fs.existsSync(cacheFile)) {
        // TODO: against dogma, confirm whether readFileSync is indeed slower
        const promise = fs.promises.readFile(cacheFile, `utf8`);
        inFlightBase64Cache.set(requestUrl, promise);
        return promise.then(body => {
            inFlightBase64Cache.delete(requestUrl);
            resolvedBase64Cache.set(requestUrl, body);
            return body;
        });
    }

    const promise = new Promise((resolve, reject) => {
        base64Img.requestBase64(requestUrl, (a, b, body) => {
            // TODO: against dogma, confirm whether writeFileSync is indeed slower
            fs.promises
                .writeFile(cacheFile, body)
                .then(() => resolve(body))
                .catch(e => {
                    console.error(
                        `Contentful:getBase64Image: failed to write ${body.length} bytes remotely fetched from \`${requestUrl}\` to: \`${cacheFile}\`\nError: ${e}`,
                    );
                    reject(e);
                });
        });
    });

    inFlightBase64Cache.set(requestUrl, promise);

    return promise.then(body => {
        inFlightBase64Cache.delete(requestUrl);
        resolvedBase64Cache.set(requestUrl, body);
        return body;
    });
};
