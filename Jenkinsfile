pipeline {
    // Agent selection
    agent any

    // Environment variables
    environment {
        YARN_CACHE_FOLDER = "/tmp/yarn-cache/${env.JOB_NAME}"
        NODE_ENV = "production"
        TITLE = "${params.TITLE}"
        GATSBY_API_BASE = "${params.GATSBY_API_BASE}"
    }

    // Select node version
    tools {
        nodejs "v10.16.0"
    }

    // Define stages
    stages {

        // Install
        stage("Install") {
            steps {
                sh "yarn install --pure-lockfile"
                sh "echo Hello Jenkins"
            }
        }

        // Clean output
        stage("Clean") {
            steps {
                sh "rm -rf ./public"
                sh "mkdir -p ./public"
            }
        }

        // Build and Distribute
        stage("Build") {
            parallel {
                stage("Pages") {
                    steps {
                        sh "yarn run build"
                    }
                }

                stage("Styleguide") {
                    steps {
                        sh "yarn run build:styleguidist"
                    }
                }

                stage("Test") {
                    steps {
                        sh "yarn run build:test"
                        sh "yarn run build:coverage"
                    }
                }
            }
        }

        // publish
        stage("Upload") {
            steps {
                withAWS(region: params.AWS_REGION, credentials: params.AWS_CREDENTIALS) {
                    s3Upload(file:'./public', bucket: params.AWS_S3_BUCKET, acl: "PublicRead")
                }
            }
        }
    }
}
