/* eslint-env node */
require("@babel/register");

module.exports = {
    siteMetadata: {
        title: process.env.TITLE,
    },
    plugins: [
        // Helmet configuration
        "gatsby-plugin-react-helmet",

        {
            resolve: "gatsby-source-contentful",
            options: {
                spaceId: process.env.GATSBY_CF_SPACE_ID,
                accessToken: process.env.GATSBY_CF_TOKEN,
                environment: process.env.GATSBY_CF_ENVIRONMENT || "master",
            },
        },

        // Configure source filesystem images path
        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "images",
                path: `${__dirname}/src/images`,
            },
        },
        "gatsby-plugin-sharp",
        "gatsby-transformer-sharp",
        "gatsby-transformer-sqip",
        "gatsby-plugin-image",

        // Favicons and web manifest
        {
            resolve: "gatsby-plugin-manifest",
            options: {
                name: process.env.TITLE,
                short_name: process.env.TITLE,
                start_url: "/",
                background_color: "#3b3b39",
                theme_color: "#3b3b39",
                display: "standalone",
                icon: "src/images/favicon.png", // This path is relative to the root of the site.
            },
        },

        // Offline functionality
        // "gatsby-plugin-offline",

        // Sass support
        {
            resolve: "gatsby-plugin-sass",
            options: {
                includePaths: [`${__dirname}/node_modules`],
            },
        },

        // Eslinting
        "gatsby-plugin-eslint",

        // Style linting
        {
            resolve: "@danbruegge/gatsby-plugin-stylelint",
            options: { files: ["src/**/*.{js,css,scss}"] },
        },
    ],
};
