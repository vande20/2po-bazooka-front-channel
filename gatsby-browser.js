//@flow

import "./src/styles/index.scss";

import React from "react";
import { Provider } from "react-redux";

import { create } from "./src/redux";

const store = create();

export const wrapRootElement = ({ element }: *) => (
    <Provider store={store}>{element}</Provider>
);
